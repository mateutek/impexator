'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ImpexdbSchema = new Schema({
  name: String,
  info: String,
  active: Boolean,
  data: String,
  tags:[{type: Schema.Types.ObjectId, ref: 'Tag' }],
  vars:[{type: Schema.Types.ObjectId, ref:'Variable'}]
});

module.exports = mongoose.model('Impexdb', ImpexdbSchema);
