'use strict';

var _ = require('lodash');
var Impexdb = require('./impexdb.model');

// Get list of impexdbs
exports.index = function(req, res) {
  Impexdb.find().populate('tags').populate('vars').exec(function (err, impexdbs) {
    if(err) { return handleError(res, err); }
    return res.json(200, impexdbs);
  });
};

// Get a single impexdb
exports.show = function(req, res) {
  Impexdb.findById(req.params.id).populate('tags').populate('vars').exec(function (err, impexdb) {
    if(err) { return handleError(res, err); }
    if(!impexdb) { return res.send(404); }
    return res.json(impexdb);
  });
};

// Creates a new impexdb in the DB.
exports.create = function(req, res) {
  Impexdb.create(req.body, function(err, impexdb) {
    if(err) { return handleError(res, err); }
    return res.json(201, impexdb);
  });
};

// Updates an existing impexdb in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  console.log(req.body);
  Impexdb.findById(req.params.id)
    .populate('vars')
    .exec( function (err, impexdb) {
    if (err) { return handleError(res, err); }
    if(!impexdb) { return res.send(404); }
    _.extend(impexdb, req.body);
    impexdb.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, impexdb);
    });
  });
};

// Deletes a impexdb from the DB.
exports.destroy = function(req, res) {
  Impexdb.findById(req.params.id, function (err, impexdb) {
    if(err) { return handleError(res, err); }
    if(!impexdb) { return res.send(404); }
    impexdb.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
