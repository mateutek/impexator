'use strict';

var _ = require('lodash');
var Variable = require('./variable.model');

// Get list of variables
exports.index = function(req, res) {
  Variable.find(function (err, variables) {
    if(err) { return handleError(res, err); }
    return res.json(200, variables);
  });
};

// Get a single variable
exports.show = function(req, res) {
  Variable.findById(req.params.id, function (err, variable) {
    if(err) { return handleError(res, err); }
    if(!variable) { return res.send(404); }
    return res.json(variable);
  });
};

// Creates a new variable in the DB.
exports.create = function(req, res) {
  Variable.create(req.body, function(err, variable) {
    if(err) { return handleError(res, err); }
    return res.json(201, variable);
  });
};

// Updates an existing variable in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Variable.findById(req.params.id, function (err, variable) {
    if (err) { return handleError(res, err); }
    if(!variable) { return res.send(404); }
    var updated = _.merge(variable, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, variable);
    });
  });
};

// Deletes a variable from the DB.
exports.destroy = function(req, res) {
  Variable.findById(req.params.id, function (err, variable) {
    if(err) { return handleError(res, err); }
    if(!variable) { return res.send(404); }
    variable.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}