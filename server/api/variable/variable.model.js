'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var VariableSchema = new Schema({
  name: {type:String,required:true},
  variable: {type:String,required:true},
  defaultValue: {type:String,required:true},
  info: String,
  active: { type: Boolean, default: true }
});

module.exports = mongoose.model('Variable', VariableSchema);
