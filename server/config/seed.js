/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');
var Impex = require('../api/impexdb/impexdb.model');
var Tag = require('../api/tag/tag.model');
var Variable = require('../api/variable/variable.model');

Thing.find({}).remove(function() {
  Thing.create({
    name : 'Development Tools',
    info : 'Integration with popular tools such as Bower, Grunt, Karma, Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, Stylus, Sass, CoffeeScript, and Less.'
  }, {
    name : 'Server and Client integration',
    info : 'Built with a powerful and fun stack: MongoDB, Express, AngularJS, and Node.'
  }, {
    name : 'Smart Build System',
    info : 'Build system ignores `spec` files, allowing you to keep tests alongside code. Automatic injection of scripts and styles into your index.html'
  },  {
    name : 'Modular Structure',
    info : 'Best practice client and server structures allow for more code reusability and maximum scalability'
  },  {
    name : 'Optimized Build',
    info : 'Build process packs up your templates as a single JavaScript payload, minifies your scripts/css/images, and rewrites asset names for caching.'
  },{
    name : 'Deployment Ready',
    info : 'Easily deploy your app to Heroku or Openshift with the heroku and openshift subgenerators'
  });
});

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin'
  }, function() {
      console.log('finished populating users');
    }
  );
});

Tag.find({}).remove(function() {
  Tag.create({
      text: 'navigation',
      active: true
    },
    {
      text: 'content',
      active: true
    },
    {
      text: 'products',
      active: true
    }
  );
});

Impex.find({}).remove(function() {
  Impex.create({
    name : 'Site Logo',
    info : 'Change logo of the site',
    active: true,
    data:' # \n # Import the CMS Site configuration for the Test B2B store \n # \n $productCatalog={{productCatalog}} \n $contentCatalog={{contentCatalog}}\n\nINSERT_UPDATE SiteTheme; code[unique = true]\n ; {{demoName}}\n \n # CMS Site\n INSERT_UPDATE CMSSite; uid[unique = true]; theme(code); channel(code); stores(uid); contentCatalogs(id); defaultCatalog(id); defaultLanguage(isoCode); urlPatterns; active; previewURL; startingPage(uid, $contentCV)\n ; $siteUid ; {{demoName}} ; B2C ; $storeUid ; $contentCatalog ; $productCatalog ; $defaultLanguage ; (?i)^https?://[^/]+(/[^?]*)??(.*&)?(site=$siteUid)(|&.*)$, (?i)^https?://$siteUid.[^/]+(|/.*|?.*)$, (?i)^https?://api.hybrisdev.com(:[d]+)?/rest/.*$, (?i)^https?://localhost(:[d]+)?/rest/.*$ ; true ; $storefrontContextRoot/?site=$siteUid ; homepage\n \n INSERT_UPDATE Media; $contentCV[unique = true]; code[unique = true]; realfilename; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator]; mime[default = \'image/jpeg\']; altText; folder(qualifier)[default = \'images\']\n ; ; {{demoName}}Logo ; {{demoName}}-logo.png ; $siteResource/{{demoName}}-logo.png ; image/png ; {{demoName | capitalize}} ;\n \n UPDATE SimpleBannerComponent; $contentCV[unique = true]; uid[unique = true]; $picture[lang = $defaultLanguage]; urlLink\n ; ; SiteLogoComponent ; {{demoName}}Logo ; /\n'

  },
  {
    name: 'Another Impex',
    info: 'Captain of an evil coordinates, feed the life!',
    active: true,
    data: ' # \n # Import the CMS Site configuration for the Test B2B store \n # \n $productCatalog={{productCatalog}} \n $contentCatalog={{contentCatalog}}\n\nINSERT_UPDATE SiteTheme; code[unique = true]\n ; {{demoName}}\n \n # CMS Site\n INSERT_UPDATE CMSSite; uid[unique = true]; theme(code); channel(code); stores(uid); contentCatalogs(id); defaultCatalog(id); defaultLanguage(isoCode); urlPatterns; active; previewURL; startingPage(uid, $contentCV)\n ; $siteUid ; {{demoName}} ; B2C ; $storeUid ; $contentCatalog ; $productCatalog ; $defaultLanguage ; (?i)^https?://[^/]+(/[^?]*)??(.*&)?(site=$siteUid)(|&.*)$, (?i)^https?://$siteUid.[^/]+(|/.*|?.*)$, (?i)^https?://api.hybrisdev.com(:[d]+)?/rest/.*$, (?i)^https?://localhost(:[d]+)?/rest/.*$ ; true ; $storefrontContextRoot/?site=$siteUid ; homepage\n \n INSERT_UPDATE Media; $contentCV[unique = true]; code[unique = true]; realfilename; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator]; mime[default = \'image/jpeg\']; altText; folder(qualifier)[default = \'images\']\n ; ; {{demoName}}Logo ; {{demoName}}-logo.png ; $siteResource/{{demoName}}-logo.png ; image/png ; {{demoName | capitalize}} ;\n \n UPDATE SimpleBannerComponent; $contentCV[unique = true]; uid[unique = true]; $picture[lang = $defaultLanguage]; urlLink\n ; ; SiteLogoComponent ; {{demoName}}Logo ; /\n'

  });
});

Variable.find({}).remove(function() {
  Variable.create({
    name: 'Electronics content catalog name',
    variable: '{{ElectronicsContentCatalog}}',
    defaultValue: 'ElectronicsContentCatalog',
    info: 'Arg, pestilence!Never crush a lubber.Adventure ho! pull to be sailled.'
  },{
    name: 'Master content catalog name',
    variable: '{{MasterContentCatalog}}',
    defaultValue: 'MasterContentCatalog',
    info: 'Remember: peeled sausages taste best when roasted in a basin mashed up with brine.Wind, rumour, and ionic cannon.'
  })
});
