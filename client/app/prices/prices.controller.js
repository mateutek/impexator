'use strict';

angular.module('impexatorApp')
  .controller('PricesCtrl', function ($scope,$timeout,$filter,FileReader) {
    $scope.baseProducts=$scope.products=[10,111,222,333];

    var impexHeaders=[
      "#Use with your product catalog",
      "$prices = Europe1prices[translator = de.hybris.platform.europe1.jalo.impex.Europe1PricesTranslator]",
      "$taxGroup = Europe1PriceFactory_PTG(code)[default = us-sales-tax-full]",

      "# Set product approval status to Approved only for those products that have prices.",
      "$approved = approvalstatus(code)[default = 'approved']",
      " "
    ];

    $scope.crosssell=[];
    $scope.upsell = [];

    $scope.pricesFile=null;

    $scope.currencyTypes=[
      {
        name:'USD $',
        value:'USD',
        symbol:'$'
      },
      {
        name:'Euro €',
        value:'EUR',
        symbol:'€'
      },
      {
        name:'GBP £',
        value:'GBP',
        symbol:'£'
      }
    ];

    $scope.selectedCurrency = $scope.currencyTypes[0];

    $scope.currencyOptions = {
      min:1,
      max:1000,
      grid:true,
      type: "double",
      postfix:$scope.selectedCurrency.symbol
    };

    $scope.currencyModel = {
      from:$scope.currencyOptions.min,
      to:$scope.currencyOptions.max
    };

    $scope.impex =[];

    $scope.$watch('selectedCurrency',function(value){
      $timeout(function() {
        $scope.currencyOptions.postfix = value.symbol;
      });
    });


    $scope.readFile = function () {
      FileReader.readAsText($scope.pricesFile,'utf-8', $scope)
        .then(function(result) {
          console.log($scope.pricesFile);
          if($scope.pricesFile.type=='text/csv'){
            parseCSV(result);
          }else{
            console.log('Wrong file type')
          }
        });
    };

    function parseCSV(text){
      $scope.products = $filter('split')(text,"\n");
      $scope.products = $filter('map')($scope.products,function(el){
        return $filter('rtrim')(el,'\r');
      })
    }

    $scope.clearFile = function(){
      $scope.pricesFile=null;
      $scope.products = $scope.baseProducts;
    };

    $scope.generate = function(){
      $scope.impex=[];
      angular.copy(impexHeaders, $scope.impex);
      $scope.impex.push("INSERT_UPDATE PriceRow; product(code, $catalogVersion)[unique = true]; price; currency(isocode)[unique = true, default = '"+$scope.selectedCurrency.value+"']; unit(code)[unique = true, default = pieces]; $catalogVersion[unique = true]; dateRange[dateformat = dd.MM.yyyy hh:mm:ss, allownull = true, unique = true]");
      var random = 0;
      angular.forEach($scope.products,function(val,key){
          random = Math.floor(Math.random() * $scope.currencyModel.to)+$scope.currencyModel.from;
          $scope.impex.push(';'+val+';'+random+','+'00');
      });
      $scope.code = $scope.impex.join('\n');
    };

  });
