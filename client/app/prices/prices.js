'use strict';

angular.module('impexatorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('prices', {
        url: '/prices',
        templateUrl: 'app/prices/prices.html',
        controller: 'PricesCtrl',
        ncyBreadcrumb:{
          label:'Product Prices'
        }
      });
  });
