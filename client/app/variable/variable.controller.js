'use strict';

angular.module('impexatorApp')
  .controller('VariableCtrl', function ($scope, $http) {
    $http.get('/api/variables/').success(function(variables){
      $scope.variables = variables;
    });
  })
  .controller('VariableDetailsCtrl',function ($scope, $http, $stateParams) {
    $http.get('/api/variables/'+$stateParams.variableId).success(function(variable) {
      $scope.variable = variable;
    });

  })
  .controller('VariableEditCtrl',function($scope, $http, $state, $stateParams) {
    if($stateParams.variableId){

      $http.get('/api/variables/'+$stateParams.variableId).success(function(variable) {
        $scope.variable = variable;
        if($scope.variable) {
          $scope.model = angular.copy($scope.variable);
        }
      });
    }
    $scope.saveVariable = function() {
      $http.put('/api/variables/'+$scope.model._id,$scope.model).success(function(variable) {
        $state.go('^');
      });
    };
  })
  .controller('VariableNewCtrl',function($scope, $http, $state) {
    $scope.model = {};
    $scope.saveVariable = function(){
      $http.post('/api/variables', $scope.model).success(function(){
        $state.go('^');
      })
    }
  });

