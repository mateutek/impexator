'use strict';

angular.module('impexatorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('variable', {
        url: '/variables',
        templateUrl: 'app/variable/variable.html',
        controller: 'VariableCtrl',
        ncyBreadcrumb:{
          label:'Variables'
        }
      })
      .state('variable.detail',{
        url: '/variable/{variableId}',
        views: {
          "@" : {
            templateUrl: 'app/variable/details.html',
            controller: 'VariableDetailsCtrl'
          }
        },
        ncyBreadcrumb: {
          label: '{{variable.name}}'
        }
      })
      .state('variable.detail.edit',{
        url: '/edit',
        views: {
          "@" : {
            templateUrl: 'app/variable/edit.html',
            controller: 'VariableEditCtrl'
          }
        },
        ncyBreadcrumb: {
          label: 'Edit'
        }
      })
      .state('variable.new',{
        url: '/new',
        views: {
          "@" : {
            templateUrl: 'app/variable/new.html',
            controller: 'VariableNewCtrl'
          }
        },
        ncyBreadcrumb: {
          label: 'New'
        }
      })
  });
