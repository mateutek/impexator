'use strict';

describe('Controller: VariableCtrl', function () {

  // load the controller's module
  beforeEach(module('impexatorApp'));

  var VariableCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VariableCtrl = $controller('VariableCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
