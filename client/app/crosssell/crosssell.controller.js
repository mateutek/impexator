'use strict';

angular.module('impexatorApp')
  .controller('CrosssellCtrl', function ($scope,FileReader,$filter) {
    $scope.baseProducts=$scope.products=[10,111,222,333];

    var impexHeaders=[
      "#Use with your product catalog",
      "INSERT_UPDATE ProductReference; source(code, $catalogVersion)[unique = true]; target(code, $catalogVersion)[unique = true]; referenceType(code); active; preselected"
      ];

    $scope.crosssell=[];
    $scope.upsell = [];

    $scope.file=null;

    $scope.selectedType = {};

    $scope.relationTypes=[
      {
        name:'Cross Selling',
        value:'CROSSSELLING'
      },
      {
        name:'Up Selling',
        value:'UPSELLING'
      },
      {
        name:'Simmilar',
        value:'SIMMILAR'
      }
    ];

    $scope.impex =[];

    $scope.readFile = function () {
      console.log('aaa');
      FileReader.readAsText($scope.file,'utf-8', $scope)
        .then(function(result) {
          if($scope.file.type=='text/csv'){
            parseCSV(result);
          }else{
            console.log('Wrong file type')
          }
        });
    };

    function parseCSV(text){
      $scope.products = $filter('split')(text,"\n");
      $scope.products = $filter('map')($scope.products,function(el){
        return $filter('rtrim')(el,'\r');
      })
    }

    $scope.clearFile = function(){
      $scope.file=null;
      $scope.products = $scope.baseProducts;
    };

    $scope.generate = function(){
      $scope.impex=[impexHeaders[0],impexHeaders[1]];
      $filter('map')($scope.products,function(el){
        angular.forEach($scope.products,function(val,key){
          if(val!=el){
            $scope.impex.push(el+';'+val+';'+$scope.selectedType.value+';true;false');
          }
        })
      });
      $scope.code = $scope.impex.join('\n');
    };

  });
