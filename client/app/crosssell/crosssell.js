'use strict';

angular.module('impexatorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('crosssell', {
        url: '/cross-sell',
        templateUrl: 'app/crosssell/crosssell.html',
        controller: 'CrosssellCtrl',
        ncyBreadcrumb:{
          label:'Cross-sell'
        }
      });
  });
