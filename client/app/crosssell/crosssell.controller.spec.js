'use strict';

describe('Controller: CrosssellCtrl', function () {

  // load the controller's module
  beforeEach(module('impexatorApp'));

  var CrosssellCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CrosssellCtrl = $controller('CrosssellCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
