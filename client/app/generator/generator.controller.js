'use strict';

angular.module('impexatorApp')
  .controller('GeneratorCtrl', function ($scope, $http, $filter, $state) {

    $http.get('api/impexdbs').success(function(impexes){
      $scope.impexList = impexes;
    });

    $scope.model = {
      type:"b2b",
      impexes:{},
      variables:{}
    };
    $scope.$watch('model.impexes',function() {
      $scope.variables = [];
      $scope.impexes = [];

      angular.forEach($scope.model.impexes, function(elementTag,key){
        if(elementTag){
          $scope.impexes.push(key);
          angular.extend($scope.variables,$filter('filter')($scope.impexList, {_id:key})[0].vars);
        }
      });
    },true);

    $scope.generatePackage = function() {
      $state.go('.done',{pkg:$scope.model});
    }

  })
  .controller('GeneratorDoneCtrl', function ($scope, $http, $filter, $stateParams) {

    $scope.params =$stateParams.pkg;


  });
