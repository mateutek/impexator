'use strict';

angular.module('impexatorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('generator', {
        url: '/generator',
        templateUrl: 'app/generator/generator.html',
        controller: 'GeneratorCtrl',
        ncyBreadcrumb:{
          label: 'Package Generator'
        }
      })
      .state('generator.done', {
        url: '/done',
        views: {
          "@" : {
            templateUrl: 'app/generator/done.html',
            controller: 'GeneratorDoneCtrl'
          }
        },
        ncyBreadcrumb:{
          label: 'Package Created'
        },
        params:{
          pkg:{}
        }
      });
  });
