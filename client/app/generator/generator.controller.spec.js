'use strict';

describe('Controller: GeneratorCtrl', function () {

  // load the controller's module
  beforeEach(module('impexatorApp'));

  var GeneratorCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GeneratorCtrl = $controller('GeneratorCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
