'use strict';

angular.module('impexatorApp')
  .controller('ImpexCtrl', function ($scope, $http) {
    $scope.message = 'Hello';

    $scope.search = {};

    $scope.impexes = [];

    $http.get('/api/impexdbs').success(function(impexes) {
      $scope.impexes = impexes;
    });

    $scope.byTags = function(element, index) {
      var tagFound = false, query = $scope.search.tags;
      if(!query) {
        return true;
      }
      angular.forEach(element.tags, function(elementTag){
        if(elementTag.text.indexOf(query) > -1 ) {
          tagFound = true;
        }
      });
      return tagFound;
    }
  })
  .controller('ImpexDetailsCtrl',function ($scope, $http, $stateParams,$interpolate) {
    $scope.code ='';
    $scope.model = {};
    $scope.model.demoName = 'impexator';
    $scope.model.productCatalog ='masterProductCatalog';
    $scope.model.contentCatalog ='masterContentCatalog';

    $scope.updateCode = function(){
      $scope.code = $interpolate($scope.impex.data)($scope.model);
    };


    $http.get('/api/impexdbs/'+$stateParams.impexId).success(function(impex) {
      $scope.impex = impex;
      $scope.code = $scope.impex.data;
      $scope.updateCode();
    });
  })
  .controller('ImpexEditCtrl',function($scope, $rootScope, $http, $state, $stateParams, $modal) {
    if($stateParams.impexId){
      $scope.stripTags = function() {
        var tmp = [];
        for(var i in $scope.model.tags){
          tmp.push($scope.model.tags[i]._id);
        }
        $scope.model.tags = tmp;
      };
      $scope.tags = [];

      $http.get('/api/impexdbs/'+$stateParams.impexId).success(function(impex) {
        $scope.impex = impex;
        if($scope.impex) {
          $scope.model = angular.copy($scope.impex);
          $scope.code = $scope.model.data;
          $scope.currentTags = angular.copy($scope.model.tags);
          $scope.stripTags();
          console.log($scope.model);
        }

      });

      $scope.getTags = function(query){
         return $http.get('/api/tags/find/'+query);
      };
    }

    $scope.tagAdded = function($tag){
      if(typeof $tag._id !=='undefined'){
        $scope.model.tags.push($tag._id);
      }
      else{
        //ADD NEW TAG TO TAGS
      }
    };
    //ADD REMOVING OF A TAG

    $scope.saveImpex = function() {
      $http.put('/api/impexdbs/'+$scope.model._id,$scope.model).success(function(impex) {
        $state.go('^');
      });
    };

    $scope.updateCode = function() {
      $scope.code = $scope.model.data;
    };

    $scope.animationsEnabled = true;

    $scope.open = function (size) {

      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'VariableContent.html',
        controller: 'VariableModalInstanceCtrl',
        size: size
      });

      modalInstance.result.then(function (varObj) {
        $rootScope.$broadcast('caretAdd', varObj.variable);
        $scope.updateCode();
        $scope.model.vars.push(varObj);
      }, function () {
        console.info('Modal dismissed at: ' + new Date());
      });
    };

  })
  .controller('ImpexNewCtrl',function($scope, $http, $state, $stateParams) {
    $scope.model = {};
    $scope.model.tags = [];

    $scope.tagAdded = function($tag){
      if(typeof $tag._id !=='undefined'){
        $scope.model.tags.push($tag._id);
      }
      else{
        //ADD NEW TAG TO TAGS
      }
    };
    //ADD REMOVING OF A TAG

    $http.get('/api/tags').success(function(tags) {
      $scope.tags = tags;
    });

    $scope.saveImpex = function(){
      $http.post('/api/impexdbs', $scope.model).success(function(){
        $state.go('^');
      })
    }
  });


angular.module('impexatorApp').controller('VariableModalInstanceCtrl', function ($scope, $modalInstance, $http, $filter) {

  $scope.model={
    radioVar:''
  };

  $http.get('/api/variables').success(function(variables) {
    $scope.variables = variables;
  });

  $scope.ok = function () {
    var chosenVar = $filter('filter')($scope.variables, function (d) {return d.variable ===$scope.model.radioVar;})[0];
    $modalInstance.close(chosenVar);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
