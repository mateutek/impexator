'use strict';

describe('Controller: ImpexCtrl', function () {

  // load the controller's module
  beforeEach(module('impexatorApp'));

  var ImpexCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ImpexCtrl = $controller('ImpexCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
