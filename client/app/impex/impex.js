'use strict';

angular.module('impexatorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('impexes', {
        url: '/impexes',
        templateUrl: 'app/impex/impex.html',
        controller: 'ImpexCtrl',
        ncyBreadcrumb: {
          label: 'Impexes'
        }
      })
      .state('impexes.detail',{
        url: '/impex/{impexId}',
        views: {
          "@" : {
            templateUrl: 'app/impex/details.html',
            controller: 'ImpexDetailsCtrl'
          }
        },
        ncyBreadcrumb: {
          label: '{{impex.name}}'
        }
      })
      .state('impexes.detail.edit',{
        url: '/edit',
        views: {
          "@" : {
            templateUrl: 'app/impex/edit.html',
            controller: 'ImpexEditCtrl'
          }
        },
        ncyBreadcrumb: {
          label: 'Edit'
        }
      })
      .state('impexes.new',{
        url: '/new',
        views: {
          "@" : {
            templateUrl: 'app/impex/new.html',
            controller: 'ImpexNewCtrl'
          }
        },
        ncyBreadcrumb: {
          label: 'New'
        }
      })
  });

