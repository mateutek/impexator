'use strict';

angular.module('impexatorApp')
  .controller('MainCtrl', function ($scope, $http, $firebaseArray) {
    $scope.favImpexes = [];

    $http.get('/api/impexdbs').success(function(favImpexes) {
      $scope.favImpexes = favImpexes;
    });

    var ref = new Firebase("https://blistering-fire-9138.firebaseio.com/");
    $scope.messages = $firebaseArray(ref);

    //ADD MESSAGE METHOD
    $scope.addMessage = function(e) {

      //LISTEN FOR RETURN KEY
      if (e.keyCode === 13 && $scope.msg) {
        //ALLOW CUSTOM OR ANONYMOUS USER NAMES
        var name = $scope.name || "anonymous";
        $scope.messages.$add({ from: name, body: $scope.msg });
        //RESET MESSAGE
        $scope.msg = "";
      }
    }

  });
