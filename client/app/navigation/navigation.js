'use strict';

angular.module('impexatorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('navigation', {
        url: '/navigation',
        templateUrl: 'app/navigation/navigation.html',
        controller: 'NavigationCtrl',
        ncyBreadcrumb:{
          label:'Navigation Generator'
        }
      });
  });
