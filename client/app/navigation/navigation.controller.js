'use strict';

angular.module('impexatorApp')
  .controller('NavigationCtrl', function ($rootScope,$scope,$modal,$filter,$timeout,$cookieStore) {
    $scope.navEdit = false;
    $scope.menuItems = {};
    $scope.mainNode = 'SiteRootNode';
    var menuModel;
    if(menuModel = $cookieStore.get('menuModel')) {
      $scope.menuModel = menuModel;
      $scope.contentCatalog = $cookieStore.get('contentCatalog');
      $scope.contentCV = $cookieStore.get('contentCV');
    } else {
      $scope.menuModel =  [{ item: { text: 'HomepageNav', url: '/'}, children: []}, { item: { text: 'b', url: '/linkB'}, children: [{ item: { text: 'c', url: '/LinkC'}, children: []}, { item: { text: 'd', url: '/LinkD'}, children: []}]}, { item: { text: 'e', url: '/LinkE'}, children: []}, { item: { text: 'f', url: '/LinkF'}, children: []}];
      $scope.contentCatalog = 'electronics';
      $scope.contentCV = 'Staged';
    }

    function isArray(arr){
      return Object.prototype.toString.call(arr) === '[object Array]';
    }

    function findInArray(array,uid){
      angular.forEach(array,function(val,key){
        if(val.$$uid == uid){
          array.splice(key,1);
        }
        if(isArray(val.children) && val.children.length > 0){
          val.children = findInArray(val.children,uid);
        }
      });

      return array;
    }


    $scope.addItem = function(newItem){
      var newNode = {
        item: newItem,
        children: []
      };
      $scope.menuModel.push(newNode);
      $scope.updateMenu();
    };

    $scope.editItem = function(item){
      $scope.navEdit = true;
      $scope.open(item);
    };

    $scope.itemRemove = function(item){
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'myRemoveModalContent.html',
        controller: 'RemoveModalInstanceCtrl',
        scope: $scope
      });

      $scope.removeItem = item;

      modalInstance.result.then(function () {
        $scope.menuModel = findInArray($scope.menuModel,item.$$uid);
        $timeout(function(){
          $rootScope.$broadcast('refreshNestable', { model: $scope.menuModel });
        },100);
      }, function () {
        console.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.updateMenu = function(){
      $scope.menuItems = [];
      var tmpItem = {};
      for (var item in $scope.menuModel){
        tmpItem.name = $scope.menuModel[item].item.text;
        tmpItem.url = $scope.menuModel[item].item.url;
        $scope.menuItems.push(tmpItem);
        tmpItem={};
      }
    };

    $scope.$watchCollection('menuModel',
      function(newValue, oldValue){
        $scope.updateMenu();
      }
    );


    $scope.animationsEnabled = true;

    $scope.open = function (item,size) {
      $scope.newItem = {};

      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'myNewModalContent.html',
        controller: 'NewModalInstanceCtrl',
        size: size,
        scope: $scope
      });

      if(typeof item !='undefined'){
        $scope.newItem = item;
      }

      modalInstance.result.then(function (res) {
        if(res.navEditing){
          $scope.updateMenu();
        }else{
          $scope.newItem = res.newItem;
          $scope.addItem(res.newItem);
        }
      }, function () {
        $scope.newItem = {};
        console.info('Modal dismissed at: ' + new Date());
      });
    };


    $scope.$watchGroup(['menuItems','contentCatalog','contentCV'],function(){
      $cookieStore.put('menuModel', $scope.menuModel);
      $cookieStore.put('contentCatalog', $scope.contentCatalog);
      $cookieStore.put('contentCV', $scope.contentCV);
      generateImpex();
    });

    var headers = {
      CMSLinkComponent:'INSERT_UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];name;url;&componentRef;target(code)[default=\'sameWindow\']',
      CMSNavigationNode:'INSERT_UPDATE CMSNavigationNode;uid[unique=true];$contentCV[unique=true];name;parent(uid, $contentCV);links(&componentRef);&nodeRef',
      NavigationBarComponent:'INSERT_UPDATE NavigationBarComponent; $contentCV[unique = true]; uid[unique = true]; name; wrapAfter; link(uid, $contentCV); styleClass; navigationNode(&nodeRef); dropDownLayout(code)[default = AUTO]; &componentRef',
      NavigationBarCollectionComponent:'INSERT_UPDATE NavigationBarCollectionComponent;$contentCV[unique=true];uid[unique=true];components(uid, $contentCV)',
      CMSSiteRootNode:';SiteRootNode;;SiteRootNode;root;;SiteRootNode'
    };

    function generateImpex(){

      $scope.impex = [];
      populateMacros();
      populateLinks($scope.menuModel);
      populateNodes($scope.menuModel,$scope.mainNode);
      populateBarComponents($scope.menuModel);
      populateNavBar();
      populateEn($scope.menuModel);
      $scope.code = $scope.impex.join('\n');

    };

    function populateMacros() {
      $scope.impex.push('$contentCatalog = '+$scope.contentCatalog+'ContentCatalog');
      $scope.impex.push("$contentCV = catalogVersion(catalog(id[default = $contentCatalog]), version[default = '"+$scope.contentCV+"'])");
      $scope.impex.push("$lang=en");
    };

    function populateLinks(array){
      $scope.impex.push('', headers.CMSLinkComponent);
      function populate(array) {
        angular.forEach(array,function(value,key){
          var itemName = $filter('decamelize')(value.item.text,' ');
          $scope.impex.push(';;'+uidize(value.item.text)+'Link'+';'+itemName+' Link'+';'+value.item.url+';'+value.item.text+'Link');
          if(value.hasOwnProperty('children') && value.children.length >0 ){
            populate(value.children);
          }
        });
      }
      populate(array);
    };

    function populateEn(array){
      $scope.impex.push('', 'UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];linkName[lang=$lang]');
      console.log(array);
      function populateLinks(array){
        angular.forEach(array,function(value,key){
          var itemName = $filter('decamelize')(value.item.text,' ');
          $scope.impex.push(';;'+uidize(value.item.text)+'Link'+';'+value.item.text);
          if(value.hasOwnProperty('children') && value.children.length >0 ){
            populateLinks(value.children);
          }
        });
      }
      populateLinks(array);
      $scope.impex.push('');
      $scope.impex.push('UPDATE CMSNavigationNode;$contentCV[unique=true];uid[unique=true];title[lang=$lang]');
      function populateNodes(array,parentUid){
        angular.forEach(array,function(value,key){
          var links, itemName = $filter('decamelize')(value.item.text,' ');
          $scope.impex.push(';;'+uidize(value.item.text)+'NavNode'+';'+value.item.text);
          if(value.hasOwnProperty('children') && value.children.length >0 ){
            $scope.impex.push(';;'+uidize(value.item.text)+'LinksNavNode'+';'+value.item.text);
            // populateNodes(value.children);
          }
        });
      }
      populateNodes(array);
    }

    function populateNodes(array,parentUid){
      $scope.impex.push('', headers.CMSNavigationNode);
      $scope.impex.push(headers.CMSSiteRootNode);
      function populate(array,parentUid){
        angular.forEach(array,function(value,key){
          var links, itemName = $filter('decamelize')(value.item.text,' ');
          $scope.impex.push(';'+uidize(value.item.text)+'NavNode'+';;'+itemName+' NavNode;'+parentUid+';;'+uidize(value.item.text)+'NavNode');
          if(value.hasOwnProperty('children') && value.children.length >0 ){
            $scope.impex.push(';'+uidize(value.item.text)+'LinksNavNode'+';;'+itemName+' Links NavNode;'+uidize(value.item.text)+'NavNode'+';'+joinRefs(value.children,'Link')+';'+value.item.text+'LinksNavNode');
            // populate(value.children,value.item.text+'NavNode');
          }
        });
      }
      populate(array,parentUid);
    };

    function populateBarComponents(array){
      $scope.impex.push('');
      $scope.impex.push(headers.NavigationBarComponent);
      angular.forEach(array,function(value,key){
        var itemName = $filter('decamelize')(value.item.text,' ');
        $scope.impex.push(';;'+uidize(value.item.text)+'BarComponent'+';'+itemName+' Bar Component;10;'+uidize(value.item.text)+'Link;;'+uidize(value.item.text)+'NavNode;;'+uidize(value.item.text)+'BarComponent');
      });
    };

    function populateNavBar() {
      $scope.impex.push('');
      $scope.impex.push(headers.NavigationBarCollectionComponent);
      $scope.impex.push(';;NavBarComponent;'+joinRefs($scope.menuModel,'BarComponent'));
    }

    function joinRefs(array,postfix) {
      return array.map(function(o){return uidize(o.item.text)+postfix})
    }

    function uidize(text) {
      return text.replace(' ', '').replace('&', 'And');
    }
  });

angular.module('impexatorApp').controller('NewModalInstanceCtrl', function ($scope, $modalInstance) {
  $scope.ok = function () {
    $modalInstance.close({navEditing:false,newItem:$scope.newItem});
  };

  $scope.update = function () {
    $modalInstance.close({navEditing:true});
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});


angular.module('impexatorApp').controller('RemoveModalInstanceCtrl', function ($scope, $modalInstance) {
  $scope.remove = function () {
    $modalInstance.close();
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
