'use strict';

angular.module('impexatorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('tags', {
        url: '/tags',
        templateUrl: 'app/tags/tags.html',
        controller: 'TagsCtrl',
        ncyBreadcrumb:{
          label:'Tags list'
        },
        params:{
          alert:{
            class:'',
            body:''
          }
        }
      })
      .state('tags.new', {
        url: '/new',
        views: {
          "@" : {
            templateUrl: 'app/tags/new.html',
            controller: 'TagsNewCtrl'
          }
        },
        ncyBreadcrumb: {
          label: 'New Tag'
        }
      });
  });
