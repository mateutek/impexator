'use strict';

angular.module('impexatorApp')
  .controller('TagsCtrl', function ($scope,$http,$stateParams) {
    $scope.alert = null;
    $http.get('/api/tags/').success(function(tags){
      $scope.tags = tags;
    });

    if($stateParams.alert.body.length >0){
      $scope.alert = $stateParams.alert;
    }
  })
  .controller('TagsNewCtrl', function ($scope,$http,$filter,$state) {
    $scope.model = {
      text:'',
      active:true
    };
    $scope.saveTag = function() {
      $scope.model.text = $filter('lowercase')($scope.model.text);
      $http.get('/api/tags/find/'+$scope.model.text).success(function(tag){
        if(tag.length > 0){
           $scope.alert={
             class:'warning',
             body:'Tag <strong>'+tag[0].text+'</strong> already exists in DB'
           }
        }else{

          $http.post('/api/tags/',$scope.model).success(function(tag) {
            var alert = {
              class:'success',
              body:'Successfully added tag: <strong>'+tag.text+'</strong>'
            };
            $state.go('^',{alert:alert});
          });
        }
      })
    }
  });
