'use strict';

describe('Filter: decamelize', function () {

  // load the filter's module
  beforeEach(module('impexatorApp'));

  // initialize a new instance of the filter before each test
  var decamelize;
  beforeEach(inject(function ($filter) {
    decamelize = $filter('decamelize');
  }));

  it('should return the input prefixed with "decamelize filter:"', function () {
    var text = 'angularjs';
    expect(decamelize(text)).toBe('decamelize filter: ' + text);
  });

});
