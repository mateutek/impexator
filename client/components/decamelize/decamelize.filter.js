'use strict';

angular.module('impexatorApp')
  .filter('decamelize', function () {
    return function (input,sep) {
      return input.replace(/([a-z\d])([A-Z])/g, '$1' + (sep || '_') + '$2').toLowerCase();;
    };
  });
