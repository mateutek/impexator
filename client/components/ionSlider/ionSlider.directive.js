'use strict';

angular.module('impexatorApp')
    .directive('ionslider', function($timeout) {
        return {
            restrict: 'E',
            scope: {
                ngModel: '=',
                options: '='
            },
            template: '<div></div>',
            replace: true,
            link: function($scope, $element, attrs) {
                (function init() {
                    var options ={};
                    angular.extend(options, $scope.options);

                    if(typeof options.onFinish === 'undefined'){
                        options.onFinish = function(sliderObj){
                            $scope.$applyAsync(function () {
                              if(options.type == 'double'){
                                $scope.ngModel.from = sliderObj.from;
                                $scope.ngModel.to = sliderObj.to;
                              }else{
                                $scope.ngModel = sliderObj.from;
                              }
                            });
                        }
                    }

                    $element.ionRangeSlider({
                        min: options.min,
                        max: options.max,
                        type: options.type,
                        prefix: options.prefix,
                        max_postfix: options.maxPostfix,
                        prettify: options.prettify,
                        grid: options.grid,
                        grid_margin: options.gridMargin,
                        postfix: options.postfix,
                        step: options.step,
                        hide_min_max: options.hideMinMax,
                        hide_from_to: options.hideFromTo,
                        from: options.from,
                        to: options.to,
                        disable: options.disable,
                        onChange: options.onChange,
                        onFinish: options.onFinish,
                        grid_num: options.gridNum,
                        grid_snap: options.gridSnap
                    });
                })();

                $scope.$watch('options.min', function(value) {
                    $timeout(function() {
                        $element.data("ionRangeSlider").update({
                            min: value
                        });
                    });
                }, true);

                $scope.$watch('options.max', function(value) {
                    $timeout(function() {
                        $element.data("ionRangeSlider").update({
                            max: value
                        });
                    });
                });

              $scope.$watch('options.postfix', function(value) {
                $timeout(function() {
                  $element.data("ionRangeSlider").update({
                    postfix: value
                  });
                });
              });

                $scope.$watch('options.disable', function(value) {
                    $timeout(function() {
                        $element.data("ionRangeSlider").update({
                            disable: value
                        });
                    });
                });
            }
        }
    });
