'use strict';

describe('Directive: caret', function () {

  // load the directive's module
  beforeEach(module('impexatorApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<caret></caret>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the caret directive');
  }));
});