'use strict';

angular.module('impexatorApp')
  .directive('fileInput', function ($parse) {
    return {
      template: "<input type='file' />",
      replace: true,
      restrict: 'EA',
      link: function (scope, element, attrs) {
        var modelGet = $parse(attrs.fileInput);
        var modelSet = modelGet.assign;
        var onChange = $parse(attrs.onChange);
        var forceReload = attrs.hasOwnProperty('forceReload');

        var updateModel = function () {
          scope.$apply(function () {
            modelSet(scope, element[0].files[0]);
            onChange(scope);
          });
        };

        var clearModel = function() {
          if(forceReload){
            element[0].value = null;
          }
        };

        element.on('change', updateModel);
        element.on('click',clearModel);
      }
    };
  });
