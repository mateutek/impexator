'use strict';

angular.module('impexatorApp')
  .directive('search', function () {
    return {
      templateUrl: 'components/search/search.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });