'use strict';

angular.module('impexatorApp')
  .controller('NavbarCtrl', function ($scope, $location, Auth ,md5) {
    $scope.menu = [
      {
        'title': 'Impexes',
        'link': '/impexes'
      },
      {
        'title': 'Navigation Generator',
        'link': '/navigation'
      },
      {
        'title': 'Package Generator',
        'link': '/generator'
      },
      {
        'title':'Tags',
        'link': '/tags'
      },
      {
        'title':'Variables',
        'link': '/variables'
      },
      {
        'title':'Products',
        'children':[
          {
            'title':'Cross-sell',
            'link': '/cross-sell'
          },
          {
            'title':'Prices',
            'link':'/prices'
          }
        ]
      }

    ];

    $scope.isCollapsed = false;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;
    $scope.currentUser = Auth.getCurrentUser();

    if(typeof $scope.currentUser.google !== 'undefined'){
      $scope.userImageUrl = $scope.currentUser.google.image.url;
      $scope.userName = $scope.currentUser.google.nickname;
    }else{
      $scope.userImageUrl = 'http://www.gravatar.com/avatar/'+md5.createHash($scope.currentUser.email || '');
      $scope.userName = $scope.currentUser.name;
    }

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });

angular.module('impexatorApp')
  .directive('navMenu', function () {
    return {
      templateUrl: 'components/navbar/menu.html',
      restrict: 'EA',
      replace:true,
      link: function (scope, element, attrs) {

      }
    };
  });
