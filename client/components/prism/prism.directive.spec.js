'use strict';

describe('Directive: prism', function () {

  // load the directive's module and view
  beforeEach(module('impexatorApp'));
  beforeEach(module('components/prism/prism.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<prism></prism>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the prism directive');
  }));
});