'use strict';

app.directive('nagPrism', ['$compile', function($compile) {
    return {
      restrict: 'A',
      transclude: true,
      scope: {
        source: '@'
      },
      link: function(scope, element, attrs, controller, transclude) {
        var target = $(element).find('code'),
          id = Math.random().toString(36).substring(7);
        target.attr('id',id);
        var copyBtn = '<a href="#" class="copy-btn" data-clipboard-target="'+id+'">Copy</a>';
        $(element).append(copyBtn);
        bindCopy();
        scope.$watch('source', function(v) {
          element.find("code").html(v);
          Prism.highlightElement(element.find("code")[0]);
        });


        transclude(function(clone) {
          if (clone.html() !== undefined) {
            element.find("code").html(clone.html());
            $compile(element.contents())(scope.$parent);
          }
        });
      },
      template: "<code></code>"
    };
  }]);

var bindCopy = function() {
  var client = new ZeroClipboard( $('.copy-btn') );
  client.on( "ready", function( readyEvent ) {
    // alert( "ZeroClipboard SWF is ready!" );

    client.on( "aftercopy", function( event ) {
      // `this` === `client`
      // `event.target` === the element that was clicked
      event.target.style.display = "none";
      alert("Copied text to clipboard: " + event.data["text/plain"] );
    } );
  } );
};
