'use strict';

angular.module('impexatorApp')
  .directive('tagList', function () {
    return {
      templateUrl: 'components/tag/tag.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        scope.limit = attrs.limit;
      }
    };
  });
