'use strict';

describe('Directive: nestable', function () {

  // load the directive's module and view
  beforeEach(module('impexatorApp'));
  beforeEach(module('components/nestable/nestable.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<nestable></nestable>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the nestable directive');
  }));
});