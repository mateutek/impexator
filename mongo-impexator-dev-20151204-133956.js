
/** impexdbs indexes **/
db.getCollection("impexdbs").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tags indexes **/
db.getCollection("tags").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** things indexes **/
db.getCollection("things").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** variables indexes **/
db.getCollection("variables").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** impexdbs records **/
db.getCollection("impexdbs").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c67"),
  "name": "Site Logo",
  "info": "Change logo of the site",
  "active": true,
  "data": " # \n # Import the CMS Site configuration for the Test B2B store \n # \n $productCatalog={{productCatalog}} \n $contentCatalog={{contentCatalog}}\n\nINSERT_UPDATE SiteTheme; code[unique = true]\n ; {{demoName}}\n \n # CMS Site\n INSERT_UPDATE CMSSite; uid[unique = true]; theme(code); channel(code); stores(uid); contentCatalogs(id); defaultCatalog(id); defaultLanguage(isoCode); urlPatterns; active; previewURL; startingPage(uid, $contentCV)\n ; $siteUid ; {{demoName}} ; B2C ; $storeUid ; $contentCatalog ; $productCatalog ; $defaultLanguage ; (?i)^https?://[^/]+(/[^?]*)??(.*&)?(site=$siteUid)(|&.*)$, (?i)^https?://$siteUid.[^/]+(|/.*|?.*)$, (?i)^https?://api.hybrisdev.com(:[d]+)?/rest/.*$, (?i)^https?://localhost(:[d]+)?/rest/.*$ ; true ; $storefrontContextRoot/?site=$siteUid ; homepage\n \n INSERT_UPDATE Media; $contentCV[unique = true]; code[unique = true]; realfilename; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator]; mime[default = 'image/jpeg']; altText; folder(qualifier)[default = 'images']\n ; ; {{demoName}}Logo ; {{demoName}}-logo.png ; $siteResource/{{demoName}}-logo.png ; image/png ; {{demoName | capitalize}} ;\n \n UPDATE SimpleBannerComponent; $contentCV[unique = true]; uid[unique = true]; $picture[lang = $defaultLanguage]; urlLink\n ; ; SiteLogoComponent ; {{demoName}}Logo ; /\n",
  "vars": [
    ObjectId("558ab0365fe7b95a65c46c6c")
  ],
  "tags": [
    ObjectId("558ab0365fe7b95a65c46c69")
  ],
  "__v": NumberInt(2)
});
db.getCollection("impexdbs").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c68"),
  "name": "Another Impex",
  "info": "Captain of an evil coordinates, feed the life!",
  "active": true,
  "data": " # \n # Import the CMS Site configuration for the Test B2B store \n # \n $productCatalog={{productCatalog}} \n $contentCatalog={{contentCatalog}}\n\nINSERT_UPDATE SiteTheme; code[unique = true]\n ; {{demoName}}\n \n # CMS Site\n INSERT_UPDATE CMSSite; uid[unique = true]; theme(code); channel(code); stores(uid); contentCatalogs(id); defaultCatalog(id); defaultLanguage(isoCode); urlPatterns; active; previewURL; startingPage(uid, $contentCV)\n ; $siteUid ; {{demoName}} ; B2C ; $storeUid ; $contentCatalog ; $productCatalog ; $defaultLanguage ; (?i)^https?://[^/]+(/[^?]*)??(.*&)?(site=$siteUid)(|&.*)$, (?i)^https?://$siteUid.[^/]+(|/.*|?.*)$, (?i)^https?://api.hybrisdev.com(:[d]+)?/rest/.*$, (?i)^https?://localhost(:[d]+)?/rest/.*$ ; true ; $storefrontContextRoot/?site=$siteUid ; homepage\n \n INSERT_UPDATE Media; $contentCV[unique = true]; code[unique = true]; realfilename; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator]; mime[default = 'image/jpeg']; altText; folder(qualifier)[default = 'images']\n ; ; {{demoName}}Logo ; {{demoName}}-logo.png ; $siteResource/{{demoName}}-logo.png ; image/png ; {{demoName | capitalize}} ;\n \n UPDATE SimpleBannerComponent; $contentCV[unique = true]; uid[unique = true]; $picture[lang = $defaultLanguage]; urlLink\n ; ; SiteLogoComponent ; {{demoName}}Logo ; /\n",
  "vars": [
    ObjectId("558ab0365fe7b95a65c46c6d"),
    ObjectId("558ab0365fe7b95a65c46c6c")
  ],
  "tags": [
    ObjectId("558ab0365fe7b95a65c46c6a")
  ],
  "__v": NumberInt(2)
});
db.getCollection("impexdbs").insert({
  "_id": ObjectId("5612497be1ef82479b0106d3"),
  "name": "Test",
  "info": "Testowy impex",
  "data": "Test impex data {MasterContentCatalog}",
  "vars": [
    ObjectId("558ab0365fe7b95a65c46c6d"),
    ObjectId("558ab0365fe7b95a65c46c6d"),
    ObjectId("558ab0365fe7b95a65c46c6c")
  ],
  "tags": [
    ObjectId("558ab0365fe7b95a65c46c69")
  ],
  "__v": NumberInt(1)
});

/** tags records **/
db.getCollection("tags").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c69"),
  "text": "navigation",
  "active": true,
  "__v": NumberInt(0)
});
db.getCollection("tags").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c6a"),
  "text": "content",
  "active": true,
  "__v": NumberInt(0)
});
db.getCollection("tags").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c6b"),
  "text": "products",
  "active": true,
  "__v": NumberInt(0)
});

/** things records **/
db.getCollection("things").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c5f"),
  "name": "Development Tools",
  "info": "Integration with popular tools such as Bower, Grunt, Karma, Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, Stylus, Sass, CoffeeScript, and Less.",
  "__v": NumberInt(0)
});
db.getCollection("things").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c60"),
  "name": "Server and Client integration",
  "info": "Built with a powerful and fun stack: MongoDB, Express, AngularJS, and Node.",
  "__v": NumberInt(0)
});
db.getCollection("things").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c61"),
  "name": "Smart Build System",
  "info": "Build system ignores `spec` files, allowing you to keep tests alongside code. Automatic injection of scripts and styles into your index.html",
  "__v": NumberInt(0)
});
db.getCollection("things").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c62"),
  "name": "Modular Structure",
  "info": "Best practice client and server structures allow for more code reusability and maximum scalability",
  "__v": NumberInt(0)
});
db.getCollection("things").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c63"),
  "name": "Optimized Build",
  "info": "Build process packs up your templates as a single JavaScript payload, minifies your scripts/css/images, and rewrites asset names for caching.",
  "__v": NumberInt(0)
});
db.getCollection("things").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c64"),
  "name": "Deployment Ready",
  "info": "Easily deploy your app to Heroku or Openshift with the heroku and openshift subgenerators",
  "__v": NumberInt(0)
});

/** variables records **/
db.getCollection("variables").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c6c"),
  "name": "Electronics content catalog name",
  "variable": "{{ElectronicsContentCatalog}}",
  "defaultValue": "ElectronicsContentCatalog",
  "info": "Arg, pestilence!Never crush a lubber.Adventure ho! pull to be sailled.",
  "active": true,
  "__v": NumberInt(0)
});
db.getCollection("variables").insert({
  "_id": ObjectId("558ab0365fe7b95a65c46c6d"),
  "name": "Master content catalog name",
  "variable": "{{MasterContentCatalog}}",
  "defaultValue": "MasterContentCatalog",
  "info": "Remember: peeled sausages taste best when roasted in a basin mashed up with brine.Wind, rumour, and ionic cannon.",
  "active": true,
  "__v": NumberInt(0)
});
