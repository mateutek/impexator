# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Boost your Demo workflow with IMPEXES database
* 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Needs MongoDB and Node.js
* Mongo port must be set to 3001, or change it in server/config/**/*.js files
* Fork -> npm install && bower install
* Check your Mongo configuration to work with app
* grunt serve
* New browser tab should open itself


### DOCS ###
* [Angular Fullstack](https://github.com/DaftMonk/generator-angular-fullstack)